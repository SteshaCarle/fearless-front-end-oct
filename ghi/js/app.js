function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="card shadow mb-10">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${starts} - ${ends}
            </div>
        </div>
    `;
}

function redAlert(errorCode) {
    return `
    <div class="alert alert-danger" role="alert">
    Oopsies, we got an error...
    </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url)
        if (!response.ok) {
            const badAlert = document.querySelector('h2')
            badAlert.innerHTML += redAlert(response.status)
        } else {
            const data = await response.json();
            let i = 0
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts).toLocaleDateString()
                    const ends = new Date(details.conference.ends).toLocaleDateString()
                    const location = details.conference.location.name
                    const html = createCard(name, description, pictureUrl, starts, ends, location)
                    const columns = [".col", ".col:nth-child(2)", ".col:nth-child(3)"]

                    const column = document.querySelector(columns[i % 3])
                    column.innerHTML += html
                }
                i++
            }
        }
    } catch (e) {
        console.error('An error occurred: ', e)
    }
});
