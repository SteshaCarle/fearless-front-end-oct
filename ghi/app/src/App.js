import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import Nav from './Nav'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container">
      {/* <AttendeesList attendees={props.attendees}/> */}
      <LocationForm />
      </div>
    </>
  );
}

export default App;
